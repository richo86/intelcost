create database prueba;
use prueba;

create table usuarios (
  usuario varchar(30) not null primary key,
  password varchar(20) not null
);
insert into usuarios (usuario,password) values (
  'Ricardo','intelcost'
);

create table goods (
  name varchar(30) not null,
  description text(300) not null,
  value int(20) not null
);
insert into goods (name,description,value) values (
  'item1','this is a sample',5
);

grant select, insert, update, delete
on prueba.*
to 'Ricardo'@'localhost' identified by 'intelcost';
