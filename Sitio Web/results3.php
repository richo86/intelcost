<?php
session_start();

//Esconder errores para el usuario
error_reporting(0);
ini_set('display_errors', 0);

if(isset($_SESSION['valid_user'])){

}else{
	//Si no se encuentra la sesion redireccionar a página principal
	header('Location: index.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>InterCost</title>

	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<!-- stylesheets css -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/animate.min.css">
	<link rel="stylesheet" type="text/css" href="css/plugin.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
  <link rel="stylesheet" href="css/et-line-font.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/vegas.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link href='https://fonts.googleapis.com/css?family=Rajdhani:400,500,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

	<script src="js/gen_validatorv31.js" type="text/javascript"></script>

</head>
<body>

<!-- preloader section -->
<section class="preloader">
	<div class="sk-circle">
     <div class="sk-circle1 sk-child"></div>
     <div class="sk-circle2 sk-child"></div>
     <div class="sk-circle3 sk-child"></div>
     <div class="sk-circle4 sk-child"></div>
     <div class="sk-circle5 sk-child"></div>
     <div class="sk-circle6 sk-child"></div>
     <div class="sk-circle7 sk-child"></div>
     <div class="sk-circle8 sk-child"></div>
     <div class="sk-circle9 sk-child"></div>
     <div class="sk-circle10 sk-child"></div>
     <div class="sk-circle11 sk-child"></div>
     <div class="sk-circle12 sk-child"></div>
     </div>
</section>
<!-- End of preloader section -->

<!-- Navigation Section  -->

  <div class="navbar navbar-default navbar" role="navigation">
    <div class="container">

      <div class="navbar-header">
        <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
        </button>
        <a href="index.html" class="navbar-brand smoothScroll">IntelCost</a>
      </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="logout.php" class="smoothscroll"><span>Sign out</span></a></li>
          </ul>
       </div>

    </div>
  </div>
<!--End of Navigation Section  -->

<!-- Welcome Section -->
<section id="about">
  <?php
	//revisar que la sesion esta activa
	if(!$_SESSION['valid_user']){
		exit();
	}

	//iniciar variables para conectar a base de datos
	$id = $_POST['id'];
	require_once('connect.php');

	//Conectar a base de datos y comprobar conexion
	$db_conn = mysqli_connect('localhost', $user, $pass, 'prueba');
	if ($db_conn->connect_error) {
		echo "<p style=\"font-size:20px;margin-left:15px;\">No se pudo conectar a la base de datos</p>";
    die("Connection failed: " . $conn->connect_error);
	}

  if(!$id){
    echo "<p style=\"font-size:20px;margin-left:15px;\">No has ingresado todos los campos</p>";
    exit;
  }

	//Borrar los datos indicados
  $query = "delete from goods
						where name='$id'";
  $resultado = $db_conn->query($query);

  if($resultado){
    echo "<p style=\"font-size:20px;margin-left:15px;\">Valores eliminados de manera correcta</p>";

  }else{
    echo "<p style=\"font-size:20px;margin-left:15px;\">Error, intentalo nuevamente</p>";
  }

	//Cerrar conexión
	$db_conn->close();
  ?>

</section>
<!-- End of Welcome Section -->

<!-- Footer Section -->
<footer>
	<div class="container">
		<div class="row">

      <svg class="svgcolor-light2" preserveAspectRatio="none" viewBox="0 0 100 102" height="100" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <path d="M0 0 L50 100 L100 0 Z"></path>
      </svg>

      <div class="col-md-4 col-sm-6">
        <h2>Creative Solutions</h2>
          <div class="wow fadeInUp" data-wow-delay="0.3s">
             <p>Bringing all of our talent at your disposal for you to build the website that you want</p>
             <p class="copyright-text">Copyright &copy; 2018 IntelCost <br>
          </div>
      </div>

      <div class="col-md-1 col-sm-1"></div>

      <div class="col-md-4 col-sm-5">
        <h2>Our Studio</h2>
        <p class="wow fadeInUp" data-wow-delay="0.6s">
					Cl. 90 #14-26<br>
          Tel: 311 5722737 <br>
          Bogotá, Colombia
        </p>
        <ul class="social-icon">
          <li><a href="#" class="fab fa-facebook wow bounceIn" data-wow-delay="0.9s"></a></li>
          <li><a href="#" class="fab fa-twitter wow bounceIn" data-wow-delay="1.2s"></a></li>
        </ul>
      </div>

		</div>
	</div>
</footer>
<!-- End of Footer Section -->

<!-- Collapsible Script -->
<script>
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}
</script>
<!-- End of Collapsible Script -->

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/vegas.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/scripts.js"></script>
<script src="js/plugin.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/gen_validatorv31.js"></script>

</body>
</html>
