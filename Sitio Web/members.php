<?php
session_start();

//Esconder errores para el usuario
error_reporting(0);
ini_set('display_errors', 0);

if(isset($_SESSION['valid_user'])){

}else{
	 //Si no se encuentra la sesion redireccionar a página principal
	 header('Location: index.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>InterCost</title>

	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<!-- stylesheets css -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/animate.min.css">
	<link rel="stylesheet" type="text/css" href="css/plugin.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
  <link rel="stylesheet" href="css/et-line-font.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/vegas.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link href='https://fonts.googleapis.com/css?family=Rajdhani:400,500,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

	<script src="js/gen_validatorv31.js" type="text/javascript"></script>

</head>
<body>

<!-- preloader section -->
<section class="preloader">
	<div class="sk-circle">
     <div class="sk-circle1 sk-child"></div>
     <div class="sk-circle2 sk-child"></div>
     <div class="sk-circle3 sk-child"></div>
     <div class="sk-circle4 sk-child"></div>
     <div class="sk-circle5 sk-child"></div>
     <div class="sk-circle6 sk-child"></div>
     <div class="sk-circle7 sk-child"></div>
     <div class="sk-circle8 sk-child"></div>
     <div class="sk-circle9 sk-child"></div>
     <div class="sk-circle10 sk-child"></div>
     <div class="sk-circle11 sk-child"></div>
     <div class="sk-circle12 sk-child"></div>
     </div>
</section>
<!-- End of preloader section -->

<!-- Navigation Section  -->
  <div class="navbar navbar-default navbar" role="navigation">
    <div class="container">

      <div class="navbar-header">
        <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
        </button>
        <a href="index.html" class="navbar-brand smoothScroll">IntelCost</a>
      </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="logout.php" class="smoothscroll"><span>Sign out</span></a></li>
          </ul>
       </div>

    </div>
  </div>
<!--End of Navigation Section  -->

<!-- Welcome Section -->
<section id="about">
	<div class="container">
		<div class="row">

			<div class="datos col-md-12 col-sm-12">
					<p>A continuación puedes ver el contenido de la tabla "goods":</p>
					<br />
					<?php
					//revisar que la sesion esta activa
					if(!$_SESSION['valid_user']){
						exit();
					}

					//importar credenciales de base de datos
					require_once('connect.php');

					//Realizar query para imprimir la tabla actual
					$query = 'select * from goods ';
					$db_conn = mysqli_connect('localhost', $user, $pass, 'prueba');
					$resultado = $db_conn->query($query);
					$num_results = $resultado->num_rows;
					echo "<table>";
					for($i=0; $i<$num_results; $i++){

						//Extraer los datos de la base de datos
						$row = $resultado->fetch_assoc();
						$name = $row['name'];
						$des = $row['description'];
						$val = $row['value'];

						// Imprimir los valores que se encuentran en la base de datos
		        echo "<tr>";
						echo "<td style=\"padding-right:0px;\"><p><strong>Name:</strong></p></td>";
		        echo "<td style=\"padding-right:60px;\">$name</td>";
						echo "<td style=\"padding-right:0px;\"><p><strong>Description:</strong></p></td>";
		        echo "<td style=\"padding-right:60px;\">$des</td>";
						echo "<td style=\"padding-right:0px;\"><p><strong>Value:</strong></p></td>";
		        echo "<td style=\"padding-right:60px;\">$val</td>";
		        echo "</tr>";
					}
					echo "</table>";
					$resultado->free();
					$db_conn->close();
					?>

					<!-- Presentar opciones al usuario -->
					<p style="margin-top:100px;margin-bottom:40px;">Que deseas hacer con esta tabla?:</p>
					<button class="collapsible">Añadir valores</button>
					<div class="content">

						<h1>Nueva entrada</h1>

						<form action="results.php" method="post">
							<table border = "0">
								<tr>
									<td>Name</td>
									<td><input type="text" name="name" maxlength="30" size="25"></td>
								</tr>
								<tr>
									<td>Description</td>
									<td><textarea name="description" rows="3" cols="28" required></textarea></td>
								</tr>
								<tr>
									<td>Value</td>
									<td><input type="text" name="value" maxlength="20" size="25"></td>
								</tr>
								<tr>
									<td colspan="2"><input type="submit" value="Guardar" style="margin-top:15px;margin-bottom:20px;"></td>
								</tr>
							</table>
						</form>
					</div>

					<button class="collapsible">Editar valores</button>
					<div class="content">

						<h1>Editar</h1>

						<form action="results2.php" method="post">
							<table border = "0">
								<tr>
									<td>Nombre del item que deseas modificar:</td>
									<td><input type="text" name="id" maxlength="30" size="25"></td>
								</tr>
								<tr>
									<td>Nuevo valor de Name</td>
									<td><input type="text" name="name" maxlength="30" size="25"></td>
								</tr>
								<tr>
									<td>Nuevo valor de Description</td>
									<td><textarea name="description" rows="3" cols="28" required></textarea></td>
								</tr>
								<tr>
									<td>Nuevo valor de Value</td>
									<td><input type="text" name="value" maxlength="20" size="25"></td>
								</tr>
								<tr>
									<td colspan="2"><input type="submit" value="Editar" style="margin-top:15px;margin-bottom:20px;"></td>
								</tr>
							</table>
						</form>

					</div>
					<button class="collapsible">Eliminar datos</button>
					<div class="content">

						<h1>Eliminar</h1>

						<form action="results3.php" method="post">
							<table border = "0">
								<tr>
									<td>Nombre del item que deseas eliminar:</td>
									<td><input type="text" name="id" maxlength="30" size="25"></td>
								</tr>
								<tr>
									<td colspan="2"><input type="submit" value="Eliminar" style="margin-top:15px;margin-bottom:20px;"></td>
								</tr>
							</table>
						</form>

					</div>
			</div>
		</div>
	</div>
</section>
<!-- End of Welcome Section -->

<!-- Footer Section -->
<footer>
	<div class="container">
		<div class="row">

      <svg class="svgcolor-light2" preserveAspectRatio="none" viewBox="0 0 100 102" height="100" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <path d="M0 0 L50 100 L100 0 Z"></path>
      </svg>

      <div class="col-md-4 col-sm-6">
        <h2>Creative Solutions</h2>
          <div class="wow fadeInUp" data-wow-delay="0.3s">
             <p>Bringing all of our talent at your disposal for you to build the website that you want</p>
             <p class="copyright-text">Copyright &copy; 2018 IntelCost <br>
          </div>
      </div>

      <div class="col-md-1 col-sm-1"></div>

      <div class="col-md-4 col-sm-5">
        <h2>Our Studio</h2>
        <p class="wow fadeInUp" data-wow-delay="0.6s">
					Cl. 90 #14-26<br>
          Tel: 311 5722737 <br>
          Bogotá, Colombia
        </p>
        <ul class="social-icon">
          <li><a href="#" class="fab fa-facebook wow bounceIn" data-wow-delay="0.9s"></a></li>
          <li><a href="#" class="fab fa-twitter wow bounceIn" data-wow-delay="1.2s"></a></li>
        </ul>
      </div>

		</div>
	</div>
</footer>
<!-- End of Footer Section -->

<!-- Collapsible Script -->
<script>
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}
</script>
<!-- End of Collapsible Script -->

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/vegas.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/scripts.js"></script>
<script src="js/plugin.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/gen_validatorv31.js"></script>

</body>
</html>
