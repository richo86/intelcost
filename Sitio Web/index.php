<?php
session_start();

//Esconder errores para el usuario
error_reporting(0);
ini_set('display_errors', 0);

//Verificar si el formulario fue enviado
if(isset($_POST['usuario']) && isset($_POST['password'])){

	//cargar las credenciales de la base de datos
	require_once('connect.php');

	//Si el usuario intento iniciar sesion realizar la conexión
	$usuario = $_POST['usuario'];
	$password = $_POST['password'];
	$db_conn = mysqli_connect('localhost', $user, $pass, 'prueba');

	if(mysqli_errno()){
		echo 'La conección falló, intentalo nuevamente';
		exit();
	}

	$query = 'select * from usuarios '
			."where usuario = '$usuario' "
			." and password = '$password'";
	$resultado = $db_conn->query($query);

	//Si las credenciales son correctas guardar la sesion y cerrar la conexion a la base de datos
	if($resultado->num_rows){
		$_SESSION['valid_user'] = $usuario;
		$resultado->free();
		$db_conn->close();
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>InterCost</title>

	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<!-- stylesheets css -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/animate.min.css">
	<link rel="stylesheet" type="text/css" href="css/plugin.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
  <link rel="stylesheet" href="css/et-line-font.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/vegas.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link href='https://fonts.googleapis.com/css?family=Rajdhani:400,500,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

	<script src="js/gen_validatorv31.js" type="text/javascript"></script>

</head>
<body>

<!-- preloader section -->
<section class="preloader">
	<div class="sk-circle">
     <div class="sk-circle1 sk-child"></div>
     <div class="sk-circle2 sk-child"></div>
     <div class="sk-circle3 sk-child"></div>
     <div class="sk-circle4 sk-child"></div>
     <div class="sk-circle5 sk-child"></div>
     <div class="sk-circle6 sk-child"></div>
     <div class="sk-circle7 sk-child"></div>
     <div class="sk-circle8 sk-child"></div>
     <div class="sk-circle9 sk-child"></div>
     <div class="sk-circle10 sk-child"></div>
     <div class="sk-circle11 sk-child"></div>
     <div class="sk-circle12 sk-child"></div>
     </div>
</section>
<!-- End of preloader section -->

<!-- Navigation Section  -->
  <div class="navbar navbar-default navbar" role="navigation">
    <div class="container">

      <div class="navbar-header">
        <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
        </button>
        <a href="index.html" class="navbar-brand smoothScroll">IntelCost</a>
      </div>

    </div>
  </div>
<!--End of Navigation Section  -->

<!-- Welcome Section -->
<section id="about">
	<div class="container">
		<div class="row">

      <div class="col-md-6 col-sm-12">
        <img src="images/about/about-img.png" class="img-responsive wow fadeInUp" alt="About">
      </div>

			<div class="col-md-6 col-sm-12">
				<?php
				//verificar si se ha iniciado sesion
				if(isset($_SESSION['valid_user'])){
					//Redireccionar al area de miembros
					header('Location: members.php');
				}else{
					if (isset($usuario)){
						echo '<h3>No fue posible iniciar sesión, intentalo nuevamente</h3>';
					}else{
						echo '<h3>Por favor inicia sesión</h3>';
					}
					//No se ha iniciado sesion, presentar formulario de autenticación
					echo '<form method="post" action="index.php">';
					echo '<table>';
					echo '<tr><td>Usuario:</td>';
					echo '<td><input type="text" name="usuario"></td></tr>';
					echo '<tr><td>Contraseña:</td>';
					echo '<td><input type="password" name="password"></td></tr>';
					echo '<tr><td colspan="2" align="center" style="padding-top:20px;">';
					echo '<input type="submit" value="Ingresar"></td></tr>';
					echo '</table></form>';
				}
				?>
			</div>

		</div>
	</div>
</section>
<!-- End of Welcome Section -->

<!-- Footer Section -->
<footer>
	<div class="container">
		<div class="row">

      <svg class="svgcolor-light2" preserveAspectRatio="none" viewBox="0 0 100 102" height="100" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <path d="M0 0 L50 100 L100 0 Z"></path>
      </svg>

      <div class="col-md-4 col-sm-6">
        <h2>Creative Solutions</h2>
          <div class="wow fadeInUp" data-wow-delay="0.3s">
             <p>Bringing all of our talent at your disposal for you to build the website that you want</p>
             <p class="copyright-text">Copyright &copy; 2018 IntelCost <br>
          </div>
      </div>

      <div class="col-md-1 col-sm-1"></div>

      <div class="col-md-4 col-sm-5">
        <h2>Our Studio</h2>
        <p class="wow fadeInUp" data-wow-delay="0.6s">
          Cl. 90 #14-26<br>
          Tel: 311 5722737 <br>
          Bogotá, Colombia
        </p>
        <ul class="social-icon">
          <li><a href="#" class="fab fa-facebook wow bounceIn" data-wow-delay="0.9s"></a></li>
          <li><a href="#" class="fab fa-twitter wow bounceIn" data-wow-delay="1.2s"></a></li>
        </ul>
      </div>

		</div>
	</div>
</footer>
<!-- End of Footer Section -->

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/vegas.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/scripts.js"></script>
<script src="js/plugin.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/gen_validatorv31.js"></script>

</body>
</html>
